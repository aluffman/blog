# README #

First readme for Blog application created in Ruby for Coursera MOOC by Aaron Luffman

### What is this repository for? ###

To hold homework assignments for the MOOC

### How do I get set up? ###

you can implement this BLOG application by following the instructions in the video lectures from :

https://class.coursera.org/webapplications-002/

### Contribution guidelines ###

Please don't mess with my homework.  This is public only to ensure it can be graded.  

### Who do I talk to? ###

owner